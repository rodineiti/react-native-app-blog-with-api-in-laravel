<h1 align="center">
  <a href="https://reactnative.dev/">
    React Native
  </a>
</h1>

<p align="center">
	<strong>Demo App Blog project React Native with Api Laravel</strong>
	<br />
	  <a href="https://gitlab.com/rodineiti/laravel-blog-with-sqlite">
	  https://gitlab.com/rodineiti/laravel-blog-with-sqlite
	  </a>
</p>


Clone the repository

    git clone git@gitlab.com:rodineiti/react-native-app-blog-with-api-in-laravel.git

Switch to the repo folder

    cd react-native-app-blog-with-api-in-laravel

Install all the dependencies using npm or yarn

    npm install
    yarn install

Start the local development server using npm or yarn

    npm start
    yarn start

<br />

Create a New project using Expo

	expo init myapp --template=blank
	
	cd myapp
	
Install react-navigation and using navigation/stack

Please check the official laravel installation guide for server requirements before you start <br />
<a href="https://reactnavigation.org/docs/getting-started/">https://reactnavigation.org/docs/getting-started/</a><br />
<a href="https://reactnavigation.org/docs/stack-navigator/">https://reactnavigation.org/docs/stack-navigator/</a>


	npm install @react-navigation/native

	expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view

	npm  install @react-navigation/stack
