import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";

export default function PostItem({ item, navigation }) {
  const imageUrl = item.image ? item.image_url : "http://placehold.it/750x300";
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate("Single", { id: item.id });
      }}
    >
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>{item.title}</Text>
          <View style={styles.headerAuthor}>
            <Text style={styles.authorCategory}>Author: {item.user.name}</Text>
            <Text style={styles.authorCategory}>
              Categria: {item.category.name}
            </Text>
            <Text style={styles.authorCategory}>Data: {item.created_at}</Text>
          </View>
        </View>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{ uri: imageUrl }} />
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{item.body}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
  header: {
    marginLeft: 5,
    marginBottom: 10,
  },
  headerAuthor: {
    flexDirection: "row",
    paddingTop: 10,
    paddingBottom: 10,
  },
  title: {
    fontSize: 16,
    color: "#000000",
    fontWeight: "bold",
  },
  authorCategory: {
    fontSize: 12,
    color: "#666666",
    marginRight: 10,
  },
  imageContainer: {},
  image: {
    width: "auto",
    height: 200,
    borderRadius: 5,
  },
  descriptionContainer: {},
  description: {
    color: "#000000",
    lineHeight: 18,
    margin: 10,
  },
});
