import React, { useState, useEffect } from "react";
import Pusher from "pusher-js/react-native";
import axios from "axios";
import { SafeAreaView, FlatList } from "react-native";
import PostItem from "../../components/PostItem";

export default function Home({ navigation }) {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getPosts();
  }, []);

  useEffect(() => {
    const pusher = new Pusher('', {
      cluster: 'us2',
      encrypted: true
    });
	
    const channel = pusher.subscribe('new-post');
    channel.bind('App\\Events\\StorePost', (data) => {
      getPosts();
    });
  },[]);  

  async function getPosts() {
    setLoading(true);

    const response = await axios.get(`http://192.168.1.105:8000/api/posts`);
    const posts = response.data.posts;

    setPosts(posts);
    setLoading(false);
  }

  return (
    <SafeAreaView>
      <FlatList
        data={posts}
        renderItem={({ item }) => (
          <PostItem item={item} navigation={navigation} />
        )}
        keyExtractor={(item) => String(item.id)}
        refreshing={loading}
        onRefresh={getPosts}
      />
    </SafeAreaView>
  );
}
